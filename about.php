<?php
require_once 'include/class.page.php';
page::startPage('About Us | easyMortgage.co.uk');
?>
<section id="application">
    <div class="container">
        <div class="row">
            <div class="col">
                <div id="application-form" class="mandatory-content">
                    <h1>About easyMortgage</h1>
                    <h2>We want to make mortgages easy</h2>
                    <p>At easyMortgage we think mortgages are often too complicated. Hard to apply, hard to understand, and too often, too expensive. Whether you are looking to release equity from your home, get the best deal on a secured loan, or get your first start on the housing ladder, easyMortgage.co.uk has a solution for you.</p>
                    <p>Part of the easy Family of Brands, the same company behind brands like easyJet, easyMoney and easyBus, we can be trusted to bring you great value deals, as simply as possible. We just want to make mortgages easy! Give us a try today.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
page::endPage();