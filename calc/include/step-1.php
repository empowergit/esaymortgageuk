<div class="form-group row">
    <label for="finance-amount" class="col-sm-4 col-form-label">I would like to borrow</label>
    <div class="col-sm-8">
        <div id="amount-contain-quote" class="form-control">
            <span>&pound;</span><input type="text" value="<?php echo $_GET['amount'];?>" name="finance-amount" id="finance-amount" required />
        </div>
        <label for="amount-not-sure" id="amount-not-sure-label">I'm not sure yet</label><input type="checkbox" name="amount-not-sure" id="amount-not-sure" value="1" />
        <p>This can be changed after approval</p>
    </div>
</div>
<div class="form-group row">
    <label for="pay-over" class="col-sm-4 col-form-label">To pay over</label>
    <div class="col-sm-8">
        <div id="term-contain-quote" class="form-control">
            <input type="text" value="<?php echo $_GET['term'];?>" name="pay-over" id="pay-over" required /><span id="pay-over-span">year(s)</span>
        </div>
    </div>
</div>
<div class="form-group row">
    <label for="title" class="col-sm-4 col-form-label">Title</label>
    <div class="col-sm-8">
        <label for="mr-title">Mr</label><input type="radio" name="title" id="mr-title" value="Mr" required />
        <label for="mrs-title">Mrs</label><input type="radio" name="title" value="Mrs" id="mrs-title" required />
        <label for="ms-title">Ms</label><input type="radio" name="title" id="ms-title" value="Ms" required />
        <label for="miss-title">Miss</label><input type="radio" name="title" id="miss-title" value="Miss" required />
    </div>
</div>
<div class="form-group row">
    <label for="first-name" class="col-sm-4 col-form-label">First name</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="first-name" id="first-name" required />
    </div>
</div>
<div class="form-group row">
    <label for="last-name" class="col-sm-4 col-form-label">Last name</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="last-name" id="last-name" required />
    </div>
</div>
<div class="form-group row">
    <label for="email" class="col-sm-4 col-form-label">Email Address</label>
    <div class="col-sm-8">
        <input type="email" class="form-control" name="email" id="email" required />
    </div>
</div>
<div class="form-group row">
    <label for="mobile-number" class="col-sm-4 col-form-label">Mobile phone number</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="mobile-number" id="mobile-number" required />
    </div>
</div>
<div class="form-group row">
    <label for="dob" class="col-sm-4 col-form-label">Date of Birth</label>
    <div class="col-sm-8">
        <select name="dob-day" class="form-control" required>
            <option value="">Day</option>
            <?php for($i=1;$i<=31;$i++): ?>
                <option><?php echo $i;?></option>
            <?php endfor ?>
        </select>
        <select name="dob-month" class="form-control" required>
            <option value="">Month</option>
            <?php for($i=1;$i<=12;$i++): ?>
                <option><?php echo date('M',strtotime('2019-'.str_pad($i,2,'0',STR_PAD_LEFT).'-01'));?></option>
            <?php endfor ?>
        </select>
        <select name="dob-year" class="form-control" required>
            <option value="">Year</option>
            <?php for($i=(date('Y')-18);$i>(date('Y')-100);$i--): ?>
                <option><?php echo $i;?></option>
            <?php endfor ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="property-value" class="col-sm-4 col-form-label">House Value</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="property-value" id="property-value" required />
    </div>
</div>
<div class="form-group row">
    <label for="mortgage-outstanding" class="col-sm-4 col-form-label">Mortgage Outstanding</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="mortgage-outstanding" id="mortgage-outstanding" required />
    </div>
</div>
<div class="form-group row">
    <label for="property-type" class="col-sm-4 col-form-label">Type of House (eg. Bungalow)</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="property-type" id="property-type" required />
    </div>
</div>
<div class="form-group row">
    <label for="ex-council" class="col-sm-4 col-form-label">Is House Ex-Council?</label>
    <div class="col-sm-8">
        <label for="ex-council-yes">Yes</label><input type="radio" name="ex-council" id="ex-council-yes" value="Yes" required />
        <label for="ex-council-no">No</label><input type="radio" name="ex-council" value="No" id="ex-council-no" required />
    </div>
</div>
<div class="form-group row">
    <label for="additional-info" class="col-sm-4 col-form-label">Additional information</label>
    <div class="col-sm-8">
        <textarea class="form-control" name="additional-info" id="additional-info"></textarea>
    </div>
</div>
<button class="btn btn-primary quote-submit">Get an online decision</button>
<script>
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    var length = $('#pay-over').val().length;
    $('#pay-over').css('width',length + 'ch');
</script>