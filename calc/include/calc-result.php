<div id="calc-result">
    <table>
        <tr class="table-border-tr">
            <th>Best available fixed rate</th>
            <td><strong><span id="fixed_rate_label"><?php echo $app->interest_rate;?></span>% APR</strong></td>
        </tr>
        <tr class="table-border-tr">
            <th>Reverts to SVR</th>
            <td><strong><?php echo $app->svr_rate;?>% APR</strong></td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>   
        </tr>
        <tr>
            <th id="calc-number-payments"><span>24 MONTHLY PAYMENTS</span><br/>DURING FIXED RATE PERIOD<br/>ON AN INTEREST ONLY BASIS</th>
            <td id="calc-monthly-cost"><span class="sign">&pound;</span><span class="pounds"></span><span class="pence"></span></td>
        </tr>
    </table>
    <a href="apply.php" class="btn btn-primary quote-link">Apply now</a>
</div>