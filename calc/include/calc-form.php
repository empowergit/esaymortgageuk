<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
<div id="calc-form">
	<label class="mb-0">Choose From 2 To 5 Years Fixed Rates</label>
    <div class="selectdiv">
	<select class="form-control" id="fixed-rates">
        <option value="1.54">2 year fix at 1.54%</option>
        <option value="1.20">5 year fix at 1.20%</option>  
    </select>
    </div>
	<!--<label class="mt-10 mb-0">Length Of Term</label>-->
    <div class="selectdiv" style="display: none;">
    <select class="form-control" id="finance-length-slider" >
            <option value="15">15 years </option> 
            <option value="20">20 years </option> 
            <option value="25">25 years </option> 
            <option value="30">30 years </option>  
    </select>
</div><br/>
    <label class="mt-10 mb-0">Amount to borrow</label>
    <div id="amount-contain" class="amount-bx">
        <span>&pound;</span><input type="text" value="100,000" name="finance-amount" id="finance-amount" />
    </div>
    <input type="range" name="finance-amount-slider" id="finance-amount-slider" min="25000" max="700000" step="1000" value="100000"><br/>
    
</div>
