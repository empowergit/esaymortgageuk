<?php
require_once 'include/class.page.php';
page::startPage('Contact Us | easyMortgage.co.uk');
?>
<section id="application">
    <div class="container">
        <div class="row">
            <div class="col">
                <div id="application-form" class="mandatory-content">
                    <h1>Contact Us</h1>
                    <p><strong>Need to get in touch? Just complete the form below and one of our advisers will get back to you as soon as possible.</strong></p>
                    <p>We are here to answer any questions you might have about our mortgage solutions.</p>
                    <form class="form form-horizontal" id="form-contact">
                        <div class="form-group">
                            <label for="name">Name</label><input type="text" name="name" id="name"  class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label><input type="email" name="email" id="email" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label><input type="text" name="phone" id="phone" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="enquiry-type">Type of enquiry</label><input type="text" name="enquiry-type" id="enquiry-type" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="question">Question or comment</label><textarea name="question" id="question" class="form-control"required></textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">Send Message</button>
                        </div>
                    </form>
                    <p><strong>Want to be a partner?</strong></p>
                    <p>If you work in the mortgage or secured loans space and would like to partner with easyMortgage.co.uk we are always interested in hearing from you. Send us an email to <a href="mailto:partners@easymortgage.co.uk">partners@easymortgage.co.uk</a></p>
                </div>
            </div>
        </div>
    </div>
</section>
    <section id="thank-you">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div id="thanks-msg">
                        <h1>Thank you for your message.</h1>
                        <p>One of our representatives will contact you.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
page::endPage();