


$( function() {
    $('input[type=checkbox],[type=radio]').checkboxradio({
        icon: false
    });
} );

function numberWithCommas(number) {
    var parts = number.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function calculate() {
    var fixed_rates= $("#fixed-rates :selected").val();

    // Get the user's input from the form. Assume it is all valid.
    // Convert interest from a percentage to a decimal, and convert from
    // an annual rate to a monthly rate. Convert payment period in years
    // to the number of monthly payments.
    var principal = $('#finance-amount-slider').val();
    
    

    
    var interest = fixed_rates/100/12;
   

    if(fixed_rates == "1.54"){
       var payments = 2*12;
    }else{
       var payments = 5*12;
    }

    //var payments = $('#finance-length-slider').val()*12;


    // Now compute the monthly payment figure, using esoteric math.
  //  var x = Math.pow(1 + interest, payments);
  //  var monthly = (principal*x*interest)/(x-1);


      var mon = (principal*fixed_rates) / 100;
      
     if(fixed_rates == "1.54"){
      var new_monthly =  mon*2;
      var monthly =  new_monthly / payments;
     }else{
      var new_monthly =  mon*5;
      var monthly =  new_monthly / payments;
     } 



    // Check that the result is a finite number. If so, display the results.
    if (!isNaN(monthly) &&
        (monthly != Number.POSITIVE_INFINITY) &&
        (monthly != Number.NEGATIVE_INFINITY)) {

        monthly=round(monthly);
        var monthly_split = monthly.toString().split('.');

        $('#calc-monthly-cost').html('<span class="sign">&pound;</span><span class="pounds">' + numberWithCommas(monthly_split[0]) + '</span><span class="pence">.' + monthly_split[1] + '</span>');
        var repayment = round(monthly * payments);
        //$('#calc-total-repayment').html('&pound;' + numberWithCommas(repayment));
        //$('#calc-total-credit-cost').html('&pound;' + round((monthly * payments) - principal));
        $('#calc-number-payments').html('<span>' + payments + ' MONTHLY PAYMENTS</span><br/>DURING FIXED RATE PERIOD<br/>ON AN INTEREST ONLY BASIS');
        $("#fixed_rate_label").html(fixed_rates);
    }
}

// This simple method rounds a number to two decimal places.
function round(x) {
    return parseFloat(Math.round(x*100)/100).toFixed(2);
}

$(document).ready(function() {

$( ".cross" ).hide();
$( ".menu" ).hide();
$( ".hamburger" ).click(function() {
$( ".menu" ).slideToggle( "slow", function() {
$( ".hamburger" ).hide();
$( ".cross" ).show();
});
});

$( ".cross" ).click(function() {
$( ".menu" ).slideToggle( "slow", function() {
$( ".cross" ).hide();
$( ".hamburger" ).show();
});
});

    calculate();
    $('.quote-link').attr('href','apply.php?amount=50000&term=5');

    var oldValue = '';

    $('#finance-amount').on('change',function() {
        var amount = $('#finance-amount').val().replace(',','');
        $('#finance-amount').val(numberWithCommas(amount));
        $('#finance-amount-slider').val(amount);
        $('.quote-link').attr('href','apply.php?amount=' + $('#finance-amount-slider').val() + '&term=' + $('#finance-length-slider').val());
        calculate();
    });

    $('#finance-amount-slider').on('change mousemove',function() {
        var amount = $('#finance-amount-slider').val();
        $('#finance-amount').val(numberWithCommas(amount));
        $('.quote-link').attr('href','apply.php?amount=' + $('#finance-amount-slider').val() + '&term=' + $('#finance-length-slider').val());
        calculate();
    });

    $('#finance-length-slider').on('change mousemove',function() {
        var text = '<span id="years">' + $('#finance-length-slider').val()+ '</span> year';
        if($('#finance-length-slider').val()>1)
        {
            text = text + 's';
        }
        $('#finance-length').html(text);
        $('.quote-link').attr('href','apply.php?amount=' + $('#finance-amount-slider').val() + '&term=' + $('#finance-length-slider').val());
        calculate();
    });

    $('#amount-not-sure').click(function()  {
        if($('#amount-not-sure').prop('checked'))  {
            oldValue = $('#finance-amount').val()
            $('#finance-amount').val('');
            $('#finance-amount').attr('required',false);
        }
        else
        {
            $('#finance-amount').val(oldValue);
            $('#finance-amount').attr('required',true);
        }
    });

    $('#finance-amount').click(function() {
        $('#finance-amount').val(oldValue);
        if($('#amount-not-sure').prop('checked'))  {
            $('#amount-not-sure').click();
        }
    });

    $('#pay-over').on('change keyup',function() {
        var length = $('#pay-over').val().length;
        $('#pay-over').css('width',length + 'ch');
    });

    $('#term-contain-quote').on('click',function() {
        $('#pay-over').focus();
    });

    $('#form-application').submit(function(e) {
        e.preventDefault();
        if($('#first-name').val()!='')
        {
            var data = $('form').serialize();
            $.post('submit.php',data);
            $('#application').hide();
            $("html, body").animate({ scrollTop: $('#thank-you').offset().top },'fast');
            $('#thank-you').show();
        }
        return false;
    });

    $('#form-contact').submit(function(e) {
        e.preventDefault();
        if($('#name').val()!='')
        {
            var data = $('form').serialize();
            $.post('contact-submit.php',data);
            $('#application').hide();
            $("html, body").animate({ scrollTop: $('#thank-you').offset().top },'fast');
            $('#thank-you').show();
        }
        return false;
    });

    $('#fixed-rates').on('change',function() {
        calculate();
    });
});