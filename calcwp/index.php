
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
           <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
            <link rel="stylesheet" href="css/range.css?e=<?php echo microtime();?>" />
            <link rel="stylesheet" href="css/style.css?e=<?php echo microtime();?>" />
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      

           <section id="hero">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-7 col-md-12">
                                <div id="calc-box">
                                    <div class="row">
                                        <div class="col" id="calc-box-left">
											<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
											<div id="calc-form">
												<label class="mb-0">Choose From 2 To 5 Years Fixed Rates</label>
											    <div class="selectdiv">
												<select class="form-control" id="fixed-rates">
											        <option value="1.54">2 year fix at 1.54%</option>
											        <option value="1.20">5 year fix at 1.20%</option>  
											    </select>
											    </div>
												<!--<label class="mt-10 mb-0">Length Of Term</label>-->
											    <div class="selectdiv" style="display: none;">
											    <select class="form-control" id="finance-length-slider" >
											            <option value="15">15 years </option> 
											            <option value="20">20 years </option> 
											            <option value="25">25 years </option> 
											            <option value="30">30 years </option>  
											    </select>
											</div><br/>
											    <label class="mt-10 mb-0">Amount to borrow</label>
											    <div id="amount-contain" class="amount-bx">
											        <span>&pound;</span><input type="text" value="100,000" name="finance-amount" id="finance-amount" />
											    </div>
											    <input type="range" name="finance-amount-slider" id="finance-amount-slider" min="25000" max="700000" step="1000" value="100000"><br/>
											    
											</div>
                                            
                                        </div>
                                        <div class="col" id="calc-box-right">
										<?php
										$interest_rate = 1.54;
										$svr_rate = 4.29;
										?>
										<div id="calc-result">
										    <table>
										        <tr class="table-border-tr">
										            <th>Best available fixed rate</th>
										            <td><strong><span id="fixed_rate_label"><?php echo $interest_rate;?></span>% APR</strong></td>
										        </tr>
										        <tr class="table-border-tr">
										            <th>Reverts to SVR</th>
										            <td><strong><?php echo $svr_rate;?>% APR</strong></td>
										        </tr>
										        <tr>
										          <td colspan="2">&nbsp;</td>   
										        </tr>
										        <tr>
										            <th id="calc-number-payments"><span>24 MONTHLY PAYMENTS</span><br/>DURING FIXED RATE PERIOD<br/>ON AN INTEREST ONLY BASIS</th>
										            <td id="calc-monthly-cost"><span class="sign">&pound;</span><span class="pounds"></span><span class="pence"></span></td>
										        </tr>
										    </table>
										    <a href="apply.php" class="btn btn-primary quote-link">Apply now</a>
										</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </section>

                 
        
        
        <script src="js/main.php?e=<?php echo microtime();?>"></script>
       