<?php
require_once 'include/class.page.php';
page::startPage('Apply | easyMortgage');
?>
<form autocomplete="off" method="post" id="form-application">
<section id="application">
    <div class="container">
        <div class="row">
            <div class="col">
                <div id="application-form">
                    <h1>Get an easy Quote</h1>
                    <h2>
                        Start with an easy quote using our simple mortgage calculator. This calculator is designed to give you an indicative quote if you are looking for a homeowner loan of up to &pound;500,000.<br/><br/>
                        Our homeowner loans are open to anyone in the UK, over the age of 18, even if you have a poor credit history.
                    </h2>
                    <?php require 'include/step-1.php'; ?>
                    <p>
                        <br/><strong>Need something else?</strong><br/>
                        If you have a different requirement, are you yet a homeowner, or want to release equity from your property, we can still help. Just contact us.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="thank-you">
    <div class="container">
        <div class="row">
            <div class="col">
                <div id="thanks-msg">
                    <h1>Thank you for your application</h1>
                    <p>One of our representatives will contact you to discuss your options.</p>
                </div>
            </div>
        </div>
    </div>
</section>
</form>
<?php
page::endPage();
