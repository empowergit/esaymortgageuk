<?php
require 'include/PHPMailer-master/src/PHPMailer.php';

use PHPMailer\PHPMailer\PHPMailer;
$mail = new PHPMailer();
$mail->setFrom('noreply@easymortgage.co.uk', 'easyMortgage');
$mail->addAddress('gordontblack@gmail.com','Gordon Black');
$mail->addAddress('gordon@ctr.co.uk','Gordon Black');
$mail->addAddress('gordon@salesconversions.co.uk','Gordon Black');
$mail->addAddress('marcus@ibooking.com','Marcus Cent');
$mail->addAddress('marcus@performous.com','Marcus Cent');
$mail->addAddress('m@rcus.co.uk','Marcus Cent');
$mail->addBCC('alistair@clickaffinity.net','Alistair Austen');
$mail->Subject = 'Details from easyMortgage';

$body = '<table>';
if($_POST['finance-amount']>0)
{
    $body  .= '<tr><td>Loan Amount:</td><td>&pound;'.$_POST['finance-amount'].'</td></tr>';
}
else
{
    $body  .= '<tr><td>Loan Amount:</td><td></td><td>Not sure</td></tr>';
}
$body .= '<tr><td>Payment over:</td><td>'.$_POST['pay-over'].' years</td></tr>';
$body .= '<tr><td>Title:</td><td>'.$_POST['title'].'</td></tr>';
$body .= '<tr><td>First name:</td><td>'.$_POST['first-name'].'</td></tr>';
$body .= '<tr><td>Last name:</td><td>'.$_POST['last-name'].'</td></tr>';
$body .= '<tr><td>Email:</td><td>'.$_POST['email'].'</td></tr>';
$body .= '<tr><td>Mobile number:</td><td>'.$_POST['mobile-number'].'</td></tr>';
$body .= '<tr><td>Date of birth:</td><td>'.$_POST['dob-day'].'/'.$_POST['dob-month'].'/'.$_POST['dob-year'].'</td></tr>';
$body .= '<tr><td>House Value:</td><td>'.$_POST['property-value'].'</td></tr>';
$body .= '<tr><td>Mortgage Outstanding:</td><td>'.$_POST['mortgage-outstanding'].'</td></tr>';
$body .= '<tr><td>Type of House:</td><td>'.$_POST['property-type'].'</td></tr>';
$body .= '<tr><td>Ex-Council:</td><td>'.$_POST['ex-council'].'</td></tr>';
$body .= '<tr><td>Additional Info:</td><td>'.$_POST['additional-info'].'</td></tr>';
$body .= '</table>';

$mail->Body = $body;
$mail->isHTML(true);
if(!$mail->send()) {
    //echo 'Message was not sent.';
    //echo 'Mailer error:</td><td> ' . $mail->ErrorInfo;
} else {
    //echo 'Message has been sent.';
}