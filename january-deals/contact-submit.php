<?php
require 'include/PHPMailer-master/src/PHPMailer.php';

use PHPMailer\PHPMailer\PHPMailer;
$mail = new PHPMailer();
$mail->setFrom('noreply@easymortgage.co.uk', 'easyMortgage');
$mail->addAddress('gordontblack@gmail.com','Gordon Black');
$mail->addAddress('gordon@ctr.co.uk','Gordon Black');
$mail->addAddress('gordon@salesconversions.co.uk','Gordon Black');
$mail->addAddress('marcus@ibooking.com','Marcus Cent');
$mail->addAddress('marcus@performous.com','Marcus Cent');
$mail->addAddress('m@rcus.co.uk','Marcus Cent');
$mail->addBCC('alistair@clickaffinity.net','Alistair Austen');
$mail->Subject = 'Contact from easyMortgage';

$body = '<table>';
$body .= '<tr><td>Name:</td><td>'.$_POST['name'].'</td></tr>';
$body .= '<tr><td>Email:</td><td>'.$_POST['email'].'</td></tr>';
$body .= '<tr><td>Phone:</td><td>'.$_POST['phone'].'</td></tr>';
$body .= '<tr><td>Type of enquiry:</td><td>'.$_POST['enquiry-type'].'</td></tr>';
$body .= '<tr><td>Message:</td><td>'.$_POST['question'].'</td></tr>';
$body .= '</table>';

$mail->Body = $body;
$mail->isHTML(true);
if(!$mail->send()) {
    //echo 'Message was not sent.';
    //echo 'Mailer error:</td><td> ' . $mail->ErrorInfo;
} else {
    //echo 'Message has been sent.';
}