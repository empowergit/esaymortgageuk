<?php
require_once "class.application.php";

class page
{
    function __construct()
    {
    }

    static function startPage($title='Looking for a better mortgage deal? Try easyMortgage.co.uk by Stelios',$hero=false)
    {
        global $app;
    ?>
        <!doctype html>
        <html lang="en">
        <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <!-- Required meta tags -->
            
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

            <title>Get a better mortgage deal from easyMortgage.co.uk. Part of the easy Family of Brands</title>
            
            <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5SQ859L');</script>
<!-- End Google Tag Manager -->

            <!-- Bootstrap CSS -->
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
           <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
            <link rel="stylesheet" href="css/range.css?e=<?php echo microtime();?>" />
            <link rel="stylesheet" href="css/style.css?e=<?php echo microtime();?>" />
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://kit.fontawesome.com/36d921e9f2.js" crossorigin="anonymous"></script>
        </head>
        <body>
            <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5SQ859L"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
            <section id="header">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <nav class="navbar navbar-light light-blue lighten-4">
                                <a class="navbar-brand" href="/"><img src="img/logo_em.png" height="52" width="265"></a>






                                <button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#menuContent" aria-controls="menuContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span><i class="fas fa-bars fa-1x"></i></span>
                                </button>
                                <div class="collapse navbar-collapse" id="menuContent">
                                    <!-- Links -->
                                    <ul class="navbar-nav mr-auto">
                                        <li class="nav-item">
                                            <a class="nav-link" href="/">Home</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="about.php">About easyMortgage</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="apply.php">Get an easy Quote</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="contact.php">Contact Us</a>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </section>
            <?php if($hero) { ?>
                <section id="hero">
                    <div class="container mob-container">
                        <div class="row align-items-center">
                            <?php /*<div class="col-lg-7 col-md-12">
                                <div id="calc-box">
                                    <div class="row">
                                        <div class="col" id="calc-box-left">
                                            <?php require_once 'include/calc-form.php'; ?>
                                        </div>
                                        <div class="col" id="calc-box-right">
                                            <?php require_once 'include/calc-result.php'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>*/?>
                            <div class="col-lg-9">
                                <div class="inner-content-banner-main">
                                    <div class="banner-inner-layout">
                                        <div class="two-year-circel">
                                            <div class="circel-inner">
                                                <div class="circel-text">
                                                    <h2>2 YR<span>fixed rate</span></h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rate-text-main">
                                            <div class="rate-text">
                                                <h2>1.21<sub>%</sub><span>initial rate</span></h2>
                                            </div>
                                        </div>
                                        <div class="rate-text-main">
                                            <div class="rate-text">
                                                <h2><sub>£</sub>441<span>per month</span></h2>
                                            </div>
                                        </div>
                                        <div class="apply-now-btn">
                                            <div class="apply-inner">
                                                <a href="apply.php" class="apply-btn" target="_blank">apply now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3" id="stelios-box">
                                <h2>We're one of the UK's most trusted barnds and aim to bring you the lowest mortgage Rates on the market today.</h2>
                            </div>
                        </div>
                    </div>
                </section>
                <?php
            }
    }

    static function midPage()
    {
    ?>
        <section id="home-owner"  class="home-mob-owner">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2>Get a super quick mortgage agreement in principle and get the lowest two year fixed rate deal on the market today.</h2>
                        <a href="apply.php" class="quote-link dark-orange-text">Apply Now</a>
                    </div>
                </div>
            </div>
        </section>
        <section id="made-easy">
            <div class="container">
                <div class="row">
                    
                    <div class="col-sm-6 col-xs-12" id="made-easy-block" >
                       
                       <img src="/img/leanpub-brands.svg" height="60" width="60">
                  
                        <h2>Mortgages made easy</h2>
                        <p>Looking for a mortgage deal you can trust? easyMortgage comes to you from the same easy Family of Brands you already know. We work with a panel of leading mortgage providers to find the best deal for you, whether you are a first-time buyer, looking to re-mortgage your existing home, or hoping to release equity to help with retirement. easyMortgage.co.uk makes it easy. Give us a try</p>
                        
                        
                    </div>
                    
                    <div class="col-sm-6 col-xs-12" id="made-easy-block" >
                        
                     <img src="/img/question-circle-solid.svg" height="55" width="55">
                       <h2>Why easyMortgage?</h2>
                        <p>We believe that great mortgage deals should be accessible to everyone. Our name and brand have always stood for trust and value. We don’t work with traditional high street banks. Instead, we work with a select panel of specialist lenders to get the best deal for you. Regardless of your circumstances. We want to make mortgages easy! We’re easyMortgage.co.uk, backed by the easy Family of Brands. </p>
                    </div>
                </div>
            </div>
        </section>
       
        <section id="home-owner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2>Are you a Homeowner?</h2>
                        <p>If you’re a homeowner we can offer you exceptionally low rates on a secured loan of up to &pound;500,00 even if you have a poor credit history.</p>
                        <br/><br/>
                        <h2>Not yet a Homeowner?</h2>
                        <p>We can offer great rates for first time buyers too.</p>
                        <p><a href="apply.php" class="quote-link light-orange-text">Apply now</a></p>
                    </div>
                </div>
            </div>
        </section>
        <section class="more-details" id="have-to-offer">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2>What do we have to offer?</h2>
                       <p> <i class="fas fa-angle-double-right"></i>&nbsp;Very fast approvals - you can have a deal within days</p>
                        <p><i class="fas fa-angle-double-right"></i>&nbsp;Flexible loans up to &pound;3m</p>
                        <p><i class="fas fa-angle-double-right"></i>&nbsp;Consolidation loans for homeowners - manage your debts the easy way</p>
                        <p><i class="fas fa-angle-double-right"></i>&nbsp;We search the whole market so you don't have to</p>
                    </div>
                </div>
            </div>
        </section>
    <?php
    }

    static function endPage()
    {
    ?>
        <section id="footer">
            <div class="container">
                <p>
                    Regulated by the Financial Conduct Authority.<br/>FCA Authorisation Number: 619673 | Data protection act: Z2874978.
                </p>
                <p class="footer-rates"><span class="big-rates">Rates from 1.55% APR</span> (Dependant on credit rating). </p>
                <p>
                    THINK CAREFULLY BEFORE SECURING OTHER DEBTS AGAINST YOUR HOME.<br/>
                    YOUR HOME MAY BE REPOSSESSED IF YOU DO NOT KEEP UP REPAYMENTS ON A MORTGAGE OR ANY OTHER DEBT SECURED ON IT
                </p>
                <ul>
                    <li><a href="terms.php">Terms and Conditions</a></li>
                    <li><a href="privacy.php">Privacy Policy</a></li>
                </ul>
            </div>
        </section>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="https://getaddress.io/js/jquery.getAddress-2.0.8.min.js"></script>
        <script src="js/main.php?e=<?php echo microtime();?>"></script>
        </body>
        </html>
    <?php
    }
}