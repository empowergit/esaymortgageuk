<div id="calc-result">
    <table>
        <tr>
            <th>Best available rate</th>
            <td><strong><?php echo $app->interest_rate;?>% APR</strong></td>
        </tr>
        <tr id="total-cost-row">
            <th>Total cost of credit</th>
            <td id="calc-total-credit-cost">&pound;99.40</td>
        </tr>
        <tr>
            <th>Total repayment</th>
            <td id="calc-total-repayment">&pound;5,099.40</td>
        </tr>
        <tr>
            <th id="calc-number-payments"><span>12 monthly</span><br/>payments of</th>
            <td id="calc-monthly-cost"><span class="sign">&pound;</span><span class="pounds">424</span><span class="pence">.95</span></td>
        </tr>
    </table>
    <a href="apply.php" class="btn btn-primary quote-link">Apply now</a>
</div>