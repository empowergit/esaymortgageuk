<?php
require_once "class.application.php";

class page
{
    function __construct()
    {
    }

    static function startPage($title='Looking for a better mortgage deal? Try easyMortgage.co.uk by Stelios',$hero=false)
    {
        global $app;
    ?>
        <!doctype html>
        <html lang="en">
        <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            <!-- Required meta tags -->
            
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

            <title>Get a better mortgage deal from easyMortgage.co.uk. Part of the easy Family of Brands</title>
            
            <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5SQ859L');</script>
<!-- End Google Tag Manager -->

            <!-- Bootstrap CSS -->
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
            <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
           <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
            <link rel="stylesheet" href="css/range.css?e=<?php echo microtime();?>" />
            <link rel="stylesheet" href="css/style.css?e=<?php echo microtime();?>" />
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://kit.fontawesome.com/36d921e9f2.js" crossorigin="anonymous"></script>
        </head>
        <body>
            <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5SQ859L"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
            <section id="header">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <nav class="navbar navbar-light light-blue lighten-4">
                                <a class="navbar-brand" href="/"><img src="img/logo_em.png" height="52" width="265"></a>






                                <button class="navbar-toggler toggler-example" type="button" data-toggle="collapse" data-target="#menuContent" aria-controls="menuContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span><i class="fas fa-bars fa-1x"></i></span>
                                </button>
                                <div class="collapse navbar-collapse" id="menuContent">
                                    <!-- Links -->
                                    <ul class="navbar-nav mr-auto">
                                        <li class="nav-item">
                                            <a class="nav-link" href="/">Home</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="about.php">About easyMortgage</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="apply.php">Get an easy Quote</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="contact.php">Contact Us</a>
                                        </li>
                                       
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </section>
            <?php if($hero) { ?>
                <section id="hero" class="hero-main">
                    <div class="container mob-container">
                        <div class="row">
                            <?php /*<div class="col-lg-7 col-md-12">
                                <div id="calc-box">
                                    <div class="row">
                                        <div class="col" id="calc-box-left">
                                            <?php require_once 'include/calc-form.php'; ?>
                                        </div>
                                        <div class="col" id="calc-box-right">
                                            <?php require_once 'include/calc-result.php'; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>*/?>
                            <div class="col-lg-8">
                                <div class="inner-content-banner-main">
                                    <div class="banner-inner-layout">
                                        <div class="row w-100 m-0 no-gutters">
                                            <div class="col-md-6">
                                                <div class="banner-inner-col-wrap"> 
                                                    <form>
                                                    <div class="form-group"> 
                                                        <label for="exampleFormControlSelect1">Choose From 2 To  5 Years Fixed Rates</label>
                                                        <select class="form-control" id="exampleFormControlSelect1">
                                                        <option value="1.54">2 year fix at 1.54%</option>
                                                        <option value="1.20">5 year fix at 1.20%</option> 
                                                        </select>
                                                    </div> 
                                                    <div class="form-group"> 
                                                     <label for="exampleFormControlSelect2">Length Of Term</label>
                                                        <select class="form-control" id="exampleFormControlSelect2">
                                                        <option value="15">15 years </option> 
                                                        <option value="20">20 years </option> 
                                                        <option value="25">25 years </option> 
                                                        <option value="30">30 years </option>  
                                                        </select>
                                                    </div> 
                                                        <div class="amount-to-borrow">
                                                            <h5>Amount to borrow</h5>  
                                                            <section class="range-slider" id="facet-price-range-slider"> 
                                                                <input name="range-1" value="100000" min="100000" max="1000000" step="1" type="range">  
                                                            </section>  
                                                        </div> 
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="banner-inner-col-wrap new-inner-wrap">
                                                    <ul>
                                                        <li>Best available fixed rate <span class="fixed-rate-new">3.65% APR</span></li>
                                                        <li>Reverts to SVR <span class="reverts-rate-new">4.29% APR</span></li> 
                                                        <li class="mt-5"><p class="orange d-block" id="calc-num-payments"><label class="m_y">24</label> MONTHLY <span>payments of</span></p>   <span class="main-price"><sup>&#163;</sup>912 <sup>.95</sup></span></li>
                                                    </ul>
                                                    <div class="text-center">
                                                        <a href="javascrit:void(0)" class="quote-link dark-orange d-block">Apply Now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="two-year-circel">
                                            <div class="circel-inner">
                                                <div class="circel-text">
                                                    <h2>2 YR<span>fixed rate</span></h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rate-text-main">
                                            <div class="rate-text">
                                                <h2>1.21<sub>%</sub><span>initial rate</span></h2>
                                            </div>
                                        </div>
                                        <div class="rate-text-main">
                                            <div class="rate-text">
                                                <h2><sub>£</sub>441<span>per month</span></h2>
                                            </div>
                                        </div>
                                        <div class="apply-now-btn">
                                            <div class="apply-inner">
                                                <a href="apply.php" class="apply-btn" target="_blank">apply now</a>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3" id="stelios-box">
                                <h2>Try our easy mortgage callulator and get a super fast agreement in principle without affecting your credit score from one of the UK's most trusted brands.</h2>
                            </div>
                        </div>
                    </div>
                </section>
                <?php
            }
    }

    static function midPage()
    {
    ?>
        <section id="home-owner"  class="home-mob-owner">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2>Get a super quick mortgage agreement in principle and get the lowest two year fixed rate deal on the market today.</h2>
                        <a href="apply.php" class="quote-link dark-orange-text">Apply Now</a>
                    </div>
                </div>
            </div>
        </section>
        <section id="made-easy">
            <div class="container">
                <div class="row">
                    
                    <div class="col-sm-6 col-xs-12" id="made-easy-block" >
                       
                       <img src="/img/leanpub-brands.svg" height="60" width="60">
                  
                        <h2>Mortgages made easy</h2>
                        <p>Looking for a mortgage deal you can trust? easyMortgage comes to you from the same easy Family of Brands you already know. We work with a panel of leading mortgage providers to find the best deal for you, whether you are a first-time buyer, looking to re-mortgage your existing home, or hoping to release equity to help with retirement. easyMortgage.co.uk makes it easy. Give us a try</p>
                        
                        
                    </div>
                    
                    <div class="col-sm-6 col-xs-12" id="made-easy-block" >
                        
                     <img src="/img/question-circle-solid.svg" height="55" width="55">
                       <h2>Why easyMortgage?</h2>
                        <p>We believe that great mortgage deals should be accessible to everyone. Our name and brand have always stood for trust and value. We don’t work with traditional high street banks. Instead, we work with a select panel of specialist lenders to get the best deal for you. Regardless of your circumstances. We want to make mortgages easy! We’re easyMortgage.co.uk, backed by the easy Family of Brands. </p>
                    </div>
                </div>
            </div>
        </section>
       
        <section id="home-owner-bottom">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2>Are you a Homeowner?</h2>
                        <p>If you’re a homeowner we can offer you exceptionally low rates on a secured loan of up to &pound;500,00 even if you have a poor credit history.</p>
                        <br/><br/>
                        <h2>Not yet a Homeowner?</h2>
                        <p>We can offer great rates for first time buyers too.</p>
                        <p><a href="apply.php" class="quote-link light-orange-text">Apply now</a></p>
                    </div>
                </div>
            </div>
        </section>
        <section class="more-details" id="have-to-offer">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2>What do we have to offer?</h2>
                       <p> <i class="fas fa-angle-double-right"></i>&nbsp;Very fast approvals - you can have a deal within days</p>
                        <p><i class="fas fa-angle-double-right"></i>&nbsp;Flexible loans up to &pound;3m</p>
                        <p><i class="fas fa-angle-double-right"></i>&nbsp;Consolidation loans for homeowners - manage your debts the easy way</p>
                        <p><i class="fas fa-angle-double-right"></i>&nbsp;We search the whole market so you don't have to</p>
                    </div>
                </div>
            </div>
        </section>
    <?php
    }

    static function endPage()
    {
    ?>
        <section id="footer">
            <div class="container">
                <p>
                    Regulated by the Financial Conduct Authority.<br/>FCA Authorisation Number: 619673 | Data protection act: Z2874978.
                </p>
                <p class="footer-rates"><span class="big-rates">Rates from 1.55% APR</span> (Dependant on credit rating). </p>
                <p>
                    THINK CAREFULLY BEFORE SECURING OTHER DEBTS AGAINST YOUR HOME.<br/>
                    YOUR HOME MAY BE REPOSSESSED IF YOU DO NOT KEEP UP REPAYMENTS ON A MORTGAGE OR ANY OTHER DEBT SECURED ON IT
                </p>
                <ul>
                    <li><a href="terms.php">Terms and Conditions</a></li>
                    <li><a href="privacy.php">Privacy Policy</a></li>
                </ul>
            </div>
        </section>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="https://getaddress.io/js/jquery.getAddress-2.0.8.min.js"></script>
        <script src="js/main.php?e=<?php echo microtime();?>"></script>

        <script>
            (function($) {
		
		"use strict";
		
		var DEBUG = false, // make true to enable debug output
			PLUGIN_IDENTIFIER = "RangeSlider";
	
		var RangeSlider = function( element, options ) {
			this.element = element;
			this.options = options || {};
			this.defaults = {
				output: {
					prefix: '', // function or string
					suffix: '', // function or string
					format: function(output){
						return output;
					}
				},
				change: function(event, obj){}
			};
			// This next line takes advantage of HTML5 data attributes
			// to support customization of the plugin on a per-element
			// basis.
			this.metadata = $(this.element).data('options');
		};

		RangeSlider.prototype = {

			////////////////////////////////////////////////////
			// Initializers
			////////////////////////////////////////////////////
			
			init: function() {
				if(DEBUG && console) console.log('RangeSlider init');
				this.config = $.extend( true, {}, this.defaults, this.options, this.metadata );

				var self = this;
				// Add the markup for the slider track
				this.trackFull = $('<div class="track track--full"></div>').appendTo(self.element);
				this.trackIncluded = $('<div class="track track--included"></div>').appendTo(self.element);
				this.inputs = [];
				
				$('input[type="range"]', this.element).each(function(index, value) {
					var rangeInput = this;
					// Add the ouput markup to the page.
					rangeInput.output = $('<output>').appendTo(self.element);
					// Get the current z-index of the output for later use
					rangeInput.output.zindex = parseInt($(rangeInput.output).css('z-index')) || 1;
					// Add the thumb markup to the page.
					rangeInput.thumb = $('<div class="slider-thumb">').prependTo(self.element);
					// Store the initial val, incase we need to reset.
					rangeInput.initialValue = $(this).val();
					// Method to update the slider output text/position
					rangeInput.update = function() {
						if(DEBUG && console) console.log('RangeSlider rangeInput.update');
						var range = $(this).attr('max') - $(this).attr('min'),
							offset = $(this).val() - $(this).attr('min'),
							pos = offset / range * 100 + '%',
							transPos = offset / range * -100 + '%',
							prefix = typeof self.config.output.prefix == 'function' ? self.config.output.prefix.call(self, rangeInput) : self.config.output.prefix,
							format = self.config.output.format($(rangeInput).val()),
							suffix = typeof self.config.output.suffix == 'function' ? self.config.output.suffix.call(self, rangeInput) : self.config.output.suffix;
						
						// Update the HTML
						$(rangeInput.output).html(prefix + '' + format + '' + suffix);
						$(rangeInput.output).css('left', pos);
						$(rangeInput.output).css('transform', 'translate('+transPos+',0)');
						
						// Update the IE hack thumbs
						$(rangeInput.thumb).css('left', pos);
						$(rangeInput.thumb).css('transform', 'translate('+transPos+',0)');
						
						// Adjust the track for the inputs
						self.adjustTrack();
					};
					
					// Send the current ouput to the front for better stacking
					rangeInput.sendOutputToFront = function() {
						$(this.output).css('z-index', rangeInput.output.zindex + 1);
					};
					
					// Send the current ouput to the back behind the other
					rangeInput.sendOutputToBack = function() {
						$(this.output).css('z-index', rangeInput.output.zindex);
					};
					
					///////////////////////////////////////////////////
					// IE hack because pointer-events:none doesn't pass the 
					// event to the slider thumb, so we have to make our own.
					///////////////////////////////////////////////////
					$(rangeInput.thumb).on('mousedown', function(event){
						// Send all output to the back
						self.sendAllOutputToBack();
						// Send this output to the front
						rangeInput.sendOutputToFront();
						// Turn mouse tracking on
						$(this).data('tracking', true);
						$(document).one('mouseup', function() {
							// Turn mouse tracking off
							$(rangeInput.thumb).data('tracking', false);
							// Trigger the change event
							self.change(event);
						});
					});
					
					// IE hack - track the mouse move within the input range
					$('body').on('mousemove', function(event){
						// If we're tracking the mouse move
						if($(rangeInput.thumb).data('tracking')) {
							var rangeOffset = $(rangeInput).offset(),
								relX = event.pageX - rangeOffset.left,
								rangeWidth = $(rangeInput).width();
							// If the mouse move is within the input area
							// update the slider with the correct value
							if(relX <= rangeWidth) {
								var val = relX/rangeWidth;
								$(rangeInput).val(val * $(rangeInput).attr('max'));
								rangeInput.update();
							}
						}
					});
					
					// Update the output text on slider change
					$(this).on('mousedown input change touchstart', function(event) {
						if(DEBUG && console) console.log('RangeSlider rangeInput, mousedown input touchstart');
						// Send all output to the back
						self.sendAllOutputToBack();
						// Send this output to the front
						rangeInput.sendOutputToFront();
						// Update the output
						rangeInput.update();
					});
					
					// Fire the onchange event 
					$(this).on('mouseup touchend', function(event){
						if(DEBUG && console) console.log('RangeSlider rangeInput, change');
						self.change(event);
					});
					
					// Add this input to the inputs array for use later
					self.inputs.push(this);
				});
				
				// Reset to set to initial values
				this.reset();
				
				// Return the instance
				return this;
			},
			
			sendAllOutputToBack: function() {
				$.map(this.inputs, function(input, index) {
					input.sendOutputToBack();
				});
			},
			
			change: function(event) {
				if(DEBUG && console) console.log('RangeSlider change', event);
				// Get the values of each input
				var values = $.map(this.inputs, function(input, index) {
					return {
						value: parseInt($(input).val()),
						min: parseInt($(input).attr('min')),
						max: parseInt($(input).attr('max'))
					};
				});
				
				// Sort the array by value, if we have 2 or more sliders
				values.sort(function(a, b) {
					return a.value - b.value;
				});
				
				// call the on change function in the context of the rangeslider and pass the values
				this.config.change.call(this, event, values);
			},
			
			reset: function() {
				if(DEBUG && console) console.log('RangeSlider reset');
				$.map(this.inputs, function(input, index) {
					$(input).val(input.initialValue);
					input.update();
				});
			},
			
			adjustTrack: function() {
				if(DEBUG && console) console.log('RangeSlider adjustTrack');
				var valueMin = Infinity,
					rangeMin = Infinity,
					valueMax = 0,
					rangeMax = 0;
				
				// Loop through all input to get min and max
				$.map(this.inputs, function(input, index) {
					var val = parseInt($(input).val()),
						min = parseInt($(input).attr('min')),
						max = parseInt($(input).attr('max'));
					
					// Get the min and max values of the inputs
					valueMin = (val < valueMin) ? val : valueMin;
					valueMax = (val > valueMax) ? val : valueMax;
					// Get the min and max possible values
					rangeMin = (min < rangeMin) ? min : rangeMin;
					rangeMax = (max > rangeMax) ? max : rangeMax;
				});
				
				// Get the difference if there are 2 range input, use max for one input.
				// Sets left to 0 for one input and adjust for 2 inputs
				if(this.inputs.length > 1) {
					this.trackIncluded.css('width', (valueMax - valueMin) / (rangeMax - rangeMin) * 100 + '%');
					this.trackIncluded.css('left', (valueMin - rangeMin) / (rangeMax - rangeMin) * 100 + '%');
				} else {
					this.trackIncluded.css('width', valueMax / (rangeMax - rangeMin) * 100 + '%');
					this.trackIncluded.css('left', '0%');
				}
				calculateInt();
			}
		};
	
		RangeSlider.defaults = RangeSlider.prototype.defaults;
		
		$.fn.RangeSlider = function(options) {
			if(DEBUG && console) console.log('$.fn.RangeSlider', options);
			return this.each(function() {
				var instance = $(this).data(PLUGIN_IDENTIFIER);
				if(!instance) {
					instance = new RangeSlider(this, options).init();
					$(this).data(PLUGIN_IDENTIFIER,instance);
				}
			});
		};
	
	}
)(jQuery);

$(document).ready(function() {
    
    var exampleFormControlSelect1 = "";
    var exampleFormControlSelect2 = "";
     $(".fixed-rate-new").html("1.54% APR" );
     //$(".fixed-rate-new").html("15" );

     $(document).on("change","#exampleFormControlSelect1",function(){
          //var firstselval = $(this).val();
          exampleFormControlSelect1 = $(this).val();
          //$(this).val('1.54').trigger('change');
        //console.log('First select val-------->'+exampleFormControlSelect1);
         calculateInt(exampleFormControlSelect1, exampleFormControlSelect2);
     });
     $(document).on("change","#exampleFormControlSelect2",function(){
          exampleFormControlSelect2 = $(this).val();
        //console.log('First select val-------->'+firstselval);
         calculateInt(exampleFormControlSelect1, exampleFormControlSelect2);
     });
     //calculateInt();
 });

function calculateInt( exampleFormControlSelect1, exampleFormControlSelect2)
{
    
    
    //$("#exampleFormControlSelect1").val($("#exampleFormControlSelect1 option:first").val()).trigger('change');
    //console.log(exampleFormControlSelect1);
   // console.log(exampleFormControlSelect2);
    
    var selectperNew = $('#exampleFormControlSelect1').val();
    var selectyearNew = $('#exampleFormControlSelect2').val();
    
    //var firstselval = $('#exampleFormControlSelect1').val();
    //console.log('First select val-------->'+firstselval);
    
    //var secondselval = $('#exampleFormControlSelect2').val();
    //console.log('Second select val------>'+firstselval);
    
    var yr = $("input[name='exampleRadios']:checked").val();
    
    var intRate = $("input[name='exampleRadios']:checked").data("rate");
    
    var amt = $("input[name='range-1']").val();
    
    var year = $("input[name='exampleRadios1']:checked").val();
    //console.log('Year->>'+year);
    
    months = year * 12; 
    
    

    // New code start 
    
    console.log('Total amount->>'+amt);
    
    $(".fixed-rate-new").html(selectperNew+"% APR" );
    
    yearlypayamountnew = parseInt(amt) / parseInt(selectyearNew);
    console.log('Year Payment->>>'+yearlypayamountnew.toFixed(2));
    
    
    
    if(selectperNew == '1.54'){
        var SelectYearBase = "2";
        var SelectMonthBase = "24";
        var SelectMonthBaseIntRate = "1.54";
    }else{
        var SelectYearBase = "5";
        var SelectMonthBase = "60";
        var SelectMonthBaseIntRate = "1.20";
    }
    
    $('.m_y').html(SelectMonthBase);
    
    var ForSelectedYearsTotal = yearlypayamountnew.toFixed(2) * SelectYearBase;
    //console.log('Total pay without int-->>>'+ForSelectedYearsTotal.toFixed(2));
    
    var FirstIntReatNew =  ForSelectedYearsTotal * SelectMonthBaseIntRate / 100;
    FirstIntReatYearNew = FirstIntReatNew.toFixed(2) * SelectYearBase;
    //console.log('First Int Reat Year->>'+FirstIntReatYearNew);
    
    var TotalPayWithIntNew = Math.round(ForSelectedYearsTotal) + parseInt(FirstIntReatYearNew);
    //console.log('Total Pay New Month-->'+TotalPayWithIntNew);
    
    var MonthlyInstallmentForSelectedYear = Math.round(TotalPayWithIntNew) / parseInt(SelectMonthBase);
    //console.log('Monthly Installment For Selected Year-->'+MonthlyInstallmentForSelectedYear.toFixed(2));
    
    var MonthlyInstallmentForSelectedYeararray = String(MonthlyInstallmentForSelectedYear.toFixed(2)).split(".");
    
    $('.main-price').html('<sup>£</sup>'+ MonthlyInstallmentForSelectedYeararray[0] +'<sup>.'+ MonthlyInstallmentForSelectedYeararray[1] +'</sup>');
   
    $('.quote-link').attr('href','apply.php?amount=' + amt + '&term=' + selectyearNew);
    // New code end
    
    //$capital = document.getElementById("capital").value;
    //$year = document.getElementById("year").value; // no. of compoundings per year
    //$interest = document.getElementById("interest").value; // no. of years
    
     
    rate = intRate / 100;  
	result = rate / 12 * Math.pow(1 + rate / 12,months) / ( Math.pow(1 + rate / 12,months) - 1) * amt;      

	total = result * 12 * year;

	total_interest = total - amt;
	
	yearlypayamount = parseInt(amt) / parseInt(year);
    //console.log('Yearly Pay Amount -->'+yearlypayamount);
    
    var firsttimepay = yearlypayamount * yr;
    //console.log('First Time Pay -->'+firsttimepay);

    var FirstIntReat =  amt * intRate / 100;
    FirstIntReatYear = FirstIntReat * yr;
    //console.log('First Int Reat Year->>'+FirstIntReatYear);
    
    var totalfirsttimepay = firsttimepay + FirstIntReatYear;
    //console.log('Total First Time Pay-->'+totalfirsttimepay);

    // 2 and 5 year rate

    $(".final_emi").html(result.toFixed(2) );
    
    $(".int_rate").html(intRate+"% APR");
    
    //$(".total_int").html("&#163;"+total_interest.toFixed(2) );
    $(".total_int").html("&#163;"+FirstIntReatYear.toFixed(2) );
    
    //$(".total_amt").html("&#163;"+total.toFixed(2) );
    $(".total_amt").html("&#163;"+totalfirsttimepay.toFixed(2) );
    
    $(".year_month").html(months);
    
    
    
    // Pending year rate
    
    // Remaining year count
    var remainingyear =  parseInt(year) - parseInt(yr);
    
    //console.log('total year '+year);
    //console.log(amt);
    
    var intRatePen = $("input[name='exampleRadios1']:checked").data("rate");
    
    var pendingAmt = amt - firsttimepay;
    //console.log('Pending Amt-->>'+pendingAmt.toFixed(2));
    
    var SecondIntReat =  (pendingAmt * intRatePen) / 100;
    SecondIntReatYear = SecondIntReat * remainingyear;
    SeonReat = SecondIntReatYear.toFixed(2);
    //console.log('Second Int Reat Year->>'+SeonReat);
    
    SecondTotalAmt = pendingAmt + SecondIntReatYear;
    //console.log('Second Total Amt -->'+Math.round(SecondTotalAmt));
    
    
    //SecondYearintamt = Math.round(SecondTotalAmt) - pendingAmt;
    //console.log('Second Year Int Amt'+SecondYearintamt);
    
    monthsPen = remainingyear * 12; 
    
    
    
    ratePen = intRatePen / 100;  
	resultPen = ratePen / 12 * Math.pow(1 + ratePen / 12,monthsPen) / ( Math.pow(1 + ratePen / 12,monthsPen) - 1) * amt;      
	totalPen = resultPen * 12 * remainingyear;
	total_interestPen = totalPen - amt;
    
    $(".int_rate_pen").html(intRatePen+"% APR");
    
    //$(".total_int_pen").html("&#163;"+total_interestPen.toFixed(2) );
    $(".total_int_pen").html("&#163;"+SeonReat );
    
    //$(".total_amt_pen").html("&#163;"+totalPen.toFixed(2) );
    $(".total_amt_pen").html("&#163;"+Math.round(SecondTotalAmt) );
    
    $(".startingyear").html('('+yr+ ' Year'+')');
    $(".pendingyear").html('('+remainingyear+ ' Year'+')');


    var monthly= $("#exampleFormControlSelect2 :selected").val();
    $('#calc-num-payments').html('<label class="m_y">' + monthly*12 + '</label> MONTHLY PAYMENTS<span>DURING FIXED RATE PERIOD</span>');
    
				
}

var rangeSlider = $('#facet-price-range-slider');
if(rangeSlider.length > 0) {
  rangeSlider.RangeSlider({
    output: {
      format: function(output){
        return output.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      },
      suffix: function(input){
        return parseInt($(input).val()) == parseInt($(input).attr('mins')) ? this.config.maxSymbol : '';
      }
    }
  });
}

        </script>
        <style type="text/css">
            #calc-num-payments{
                font-size: 14px;
            }
        </style>
        </body>
        </html>
    <?php
    }
}