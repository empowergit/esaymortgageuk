<?php
require '../include/class.application.php';
?>
$('#postcode_lookup').getAddress({
    api_key: '47Cu9iR0fUCN9aM7pXgt0A19561',
    button_label: 'Find address',
    output_fields:{
        line_1: '#address-line-1',
        line_2: '#address-line-2',
        line_3: '#address-line-3',
        post_town: '#city',
        county: '#county',
        postcode: '#postcode'
    },
    <!--  Optionally register callbacks at specific stages -->
    onLookupSuccess: function(data){/* Your custom code */},
    onLookupError: function(){/* Your custom code */},
    onAddressSelected: function(elem,index){/* Your custom code */}
});

$( function() {
    $('input[type=checkbox],[type=radio]').checkboxradio({
        icon: false
    });
} );

function numberWithCommas(number) {
    var parts = number.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function calculate() {

    // Get the user's input from the form. Assume it is all valid.
    // Convert interest from a percentage to a decimal, and convert from
    // an annual rate to a monthly rate. Convert payment period in years
    // to the number of monthly payments.
    var principal = $('#finance-amount-slider').val()
    var interest = <?php echo $app->interest_rate;?>/100/12;
    var payments = $('#finance-length-slider').val()*12;

    // Now compute the monthly payment figure, using esoteric math.
    var x = Math.pow(1 + interest, payments);
    var monthly = (principal*x*interest)/(x-1);

    

    // Check that the result is a finite number. If so, display the results.
    if (!isNaN(monthly) &&
        (monthly != Number.POSITIVE_INFINITY) &&
        (monthly != Number.NEGATIVE_INFINITY)) {



        monthly=round(monthly);
        var monthly_split = monthly.toString().split('.');

        $('#calc-monthly-cost').html('<span class="sign">&pound;</span><span class="pounds">' + numberWithCommas(monthly_split[0]) + '</span><span class="pence">.' + monthly_split[1] + '</span>');
        var repayment = round(monthly * payments);
        $('#calc-total-repayment').html('&pound;' + numberWithCommas(repayment));
        $('#calc-total-credit-cost').html('&pound;' + round((monthly * payments) - principal));
        $('#calc-number-payments').html('<span>' + $('#finance-length-slider').val()*12 + ' monthly</span><br/>payments of');
    }
}

// This simple method rounds a number to two decimal places.
function round(x) {
    return parseFloat(Math.round(x*100)/100).toFixed(2);
}

$(document).ready(function() {

$( ".cross" ).hide();
$( ".menu" ).hide();
$( ".hamburger" ).click(function() {
$( ".menu" ).slideToggle( "slow", function() {
$( ".hamburger" ).hide();
$( ".cross" ).show();
});
});

$( ".cross" ).click(function() {
$( ".menu" ).slideToggle( "slow", function() {
$( ".cross" ).hide();
$( ".hamburger" ).show();
});
});

    calculate();
    $('.quote-link').attr('href','apply.php?amount=50000&term=5');

    var oldValue = '';

    $('#finance-amount').on('change',function() {
        var amount = $('#finance-amount').val().replace(',','');
        $('#finance-amount').val(numberWithCommas(amount));
        $('#finance-amount-slider').val(amount);
        $('.quote-link').attr('href','apply.php?amount=' + $('#finance-amount-slider').val() + '&term=' + $('#finance-length-slider').val());
        calculate();
    });

    $('#finance-amount-slider').on('change mousemove',function() {
        var amount = $('#finance-amount-slider').val();
        $('#finance-amount').val(numberWithCommas(amount));
        $('.quote-link').attr('href','apply.php?amount=' + $('#finance-amount-slider').val() + '&term=' + $('#finance-length-slider').val());
        calculate();
    });

    $('#finance-length-slider').on('change mousemove',function() {
        var text = '<span id="years">' + $('#finance-length-slider').val()+ '</span> year';
        if($('#finance-length-slider').val()>1)
        {
            text = text + 's';
        }
        $('#finance-length').html(text);
        $('.quote-link').attr('href','apply.php?amount=' + $('#finance-amount-slider').val() + '&term=' + $('#finance-length-slider').val());
        calculate();
    });

    $('#amount-not-sure').click(function()  {
        if($('#amount-not-sure').prop('checked'))  {
            oldValue = $('#finance-amount').val()
            $('#finance-amount').val('');
            $('#finance-amount').attr('required',false);
        }
        else
        {
            $('#finance-amount').val(oldValue);
            $('#finance-amount').attr('required',true);
        }
    });

    $('#finance-amount').click(function() {
        $('#finance-amount').val(oldValue);
        if($('#amount-not-sure').prop('checked'))  {
            $('#amount-not-sure').click();
        }
    });

    $('#pay-over').on('change keyup',function() {
        var length = $('#pay-over').val().length;
        $('#pay-over').css('width',length + 'ch');
    });

    $('#term-contain-quote').on('click',function() {
        $('#pay-over').focus();
    });

    $('#form-application').submit(function(e) {
        e.preventDefault();
        if($('#first-name').val()!='')
        {
            var data = $('form').serialize();
            $.post('submit.php',data);
            $('#application').hide();
            $("html, body").animate({ scrollTop: $('#thank-you').offset().top },'fast');
            $('#thank-you').show();
        }
        return false;
    });

    $('#form-contact').submit(function(e) {
        e.preventDefault();
        if($('#name').val()!='')
        {
            var data = $('form').serialize();
            $.post('contact-submit.php',data);
            $('#application').hide();
            $("html, body").animate({ scrollTop: $('#thank-you').offset().top },'fast');
            $('#thank-you').show();
        }
        return false;
    });
});