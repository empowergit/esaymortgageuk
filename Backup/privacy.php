<?php
require_once 'include/class.page.php';
page::startPage('Privacy Policy | easyMortgage.co.uk');
?>
    <section id="application">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div id="application-form" class="mandatory-content">
                        <h1>Privacy Policy</h1>
                        
                        
                        <p>The policy: This privacy policy notice is served by [easyMortgage.co.uk] under the website; [https://www.easymortgage.co.uk]. The purpose of this policy is to explain to you how we control, process, handle and protect your personal information through the business and while you browse or use this website. If you do not agree to the following policy you may wish to cease viewing / using this website, and or refrain from submitting your personal data to us.</p>

<p>Policy key definitions:</p>

<p>"I", "our", "us", or "we" refer to the business, [easyMortgage.co.uk and Atlas Finance].</p>
<p>"you", "the user" refer to the person(s) using this website.</p>
<p>GDPR means General Data Protection Act.</p>
<p>PECR means Privacy & Electronic Communications Regulation.</p>
<p>ICO means Information Commissioner's Office.</p>
<p>Cookies mean small files stored on a users computer or device.</p>
<p>Key principles of GDPR:</p>

Our privacy policy embodies the following key principles; (a) Lawfulness, fairness and transparency, (b) Purpose limitation, (c) Data minimisation, (d) Accuracy, (e) Storage limitation, (f) Integrity and confidence, (g) Accountability.</p>

<p>Processing of your personal data</p>
<p>Under the GDPR (General Data Protection Regulation) we control and / or process any personal information about you electronically using the following lawful bases.</p>



Data retention period: We will continue to process your information under this basis until you withdraw consent or it is determined your consent no longer exists.</p>
Sharing your information: We do not share your information with third parties. / We do share your personal information with third parties and they include;</p> .
<p>Lawful basis: Contract</p>

<p>Data retention period: We will continue to process your information under this basis until you withdraw consent or it is determined your consent no longer exists.</p>
Sharing your information: We do not share your information with third parties. / We do share your personal information with third parties and they include; .
Lawful basis: Legal obligation</p>
</p>
Sharing your information: We do not share your information with third parties. / We do share your personal information with third parties and they include;</p> .
Lawful basis: Public task</p>

<p>Data retention period: We will continue to process your information under this basis until you withdraw consent or it is determined your consent no longer exists.</p>

<p>If, as determined by us, the lawful basis upon which we process your personal information changes, we will notify you about the change and any new lawful basis to be used if required. We shall stop processing your personal information if the lawful basis used is no longer relevant.</p>

<p>Your individual rights</p>
<p>Under the GDPR your rights are as follows. You can read more about your rights in details here;</p>

<p>the right to be informed;</p>
<p>the right of access;</p>
<p>the right to rectification;</p>
<p>the right to erasure;</p>
<p>the right to restrict processing;</p>
<p>the right to data portability;</p>
<p>the right to object; and</p>
<p>the right not to be subject to automated decision-making including profiling.</p>
<p>You also have the right to complain to the ICO [www.ico.org.uk] if you feel there is a problem with the way we are handling your data.</p>

<p>We handle subject access requests in accordance with the GDPR.</p>

<p>Internet cookies</p>
<p>We use cookies on this website to provide you with a better user experience. We do this by placing a small text file on your device / computer hard drive to track how you use the website, to record or log whether you have seen particular messages that we display, to keep you logged into the website where applicable, to display relevant adverts or content, referred you to a third party website.</p>

<p>Some cookies are required to enjoy and use the full functionality of this website.</p>

<p>We use a cookie control system which allows you to accept the use of cookies, and control which cookies are saved to your device / computer. Some cookies will be saved for specific time periods, where others may last indefinitely. Your web browser should provide you with the controls to manage and delete cookies from your device, please see your web browser options.</p>


<p>Data security and protection</p>
<p>We ensure the security of any personal information we hold by using secure data storage technologies and precise procedures in how we store, access and manage that information. Our methods meet the GDPR compliance requirement.</p>

Fair & Transparent Privacy Explained</p>
We have provided some further explanations about user privacy and the way we use this website to help promote a transparent and honest user privacy methodology.</p>

<p>Sponsored links, affiliate tracking & commissions</p>
Our website may contain adverts, sponsored and affiliate links on some pages. These are typically served through our advertising partners; Google Adsense, eBay Partner Network, Amazon Affiliates, or are self served through our own means. We only use trusted advertising partners who each have high standards of user privacy and security. However we do not control the actual adverts seen / displayed by our advertising partners. Our ad partners may collect data and use cookies for ad personalisation and measurement. Where ad preferences are requested as 'non-personalised' cookies may still be used for frequency capping, aggregated ad reporting and to combat fraud and abuse.


<p>Clicking on any adverts, sponsored or affiliate links may track your actions by using a cookie saved to your device. You can read more about cookies on this website above. Your actions are usually recorded as a referral from our website by this cookie. In most cases we earn a very small commission from the advertiser or advertising partner, at no cost to you, whether you make a purchase on their website or not.</p>

<p>We use advertising partners in these ways to help generate an income from the website, which allows us to continue our work and provide you with the best overall experience and valued information.</p>

If you have any concerns about this we suggest you do not click on any adverts, sponsored or affiliate links found throughout the website.</p>

<p>Email marketing messages & subscription</p>
<p>Under the GDPR we use the consent lawful basis for anyone subscribing to our newsletter or marketing mailing list. We only collect certain data about you, as detailed in the "Processing of your personal data" above. Any email marketing messages we send are done so through an EMS, email marketing service provider. An EMS is a third party service provider of software / applications that allows marketers to send out email marketing campaigns to a list of users.</p>

<p>Email marketing messages that we send may contain tracking beacons / tracked clickable links or similar server technologies in order to track subscriber activity within email marketing messages. Where used, such marketing messages may record a range of data such as; times, dates, I.P addresses, opens, clicks, forwards, geographic and demographic data. Such data, within its limitations will show the activity each subscriber made for that email campaign.</p>

<p>Any email marketing messages we send are in accordance with the GDPR and the PECR. We provide you with an easy method to withdraw your consent (unsubscribe) or manage your preferences / the information we hold about you at any time. See any marketing messages for instructions on how to unsubscribe or manage your preferences, you can also unsubscribe from all mailing lists, by following this link, otherwise contact the EMS provider.

</p>


                        
                        
                        
                    </div>
            </div>
        </div>
    </div>
</section>
<?php
page::endPage();