<div id="calc-form">
    Amount to borrow<br/>
    <div id="amount-contain">
        <span>&pound;</span><input type="text" value="50,000" name="finance-amount" id="finance-amount" />
    </div>
    <input type="range" name="finance-amount-slider" id="finance-amount-slider" min="50000" max="500000" step="1000" value="50000"><br/>
    To pay over<br/>
    <span id="finance-length"><span id="years">5</span> years</span><br/>
    <input type="range" name="finance-length-slider" id="finance-length-slider" min="5" max="40" step="1" value="1" /><br/>
</div>